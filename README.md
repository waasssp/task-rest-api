<h2>Тестовый модуль для вывода данных в формате JSON (REST API)</h2>
<h3>Endpoints:</h3>
<ul>
<li>Список всех элементов /api/address/list/</li>
<li>Получить элемент по id /api/address/get/$id/</li>
<li>Добавить новый элемент /api/address/add/<br>
    Тип запроса POST, параметры <i>name</i> и <i>address</i></li>
<li>Обновить элемент /api/address/update/<br>
   Тип запроса POST, параметры <i>id</i>, <i>name</i> и <i>address</i></li>
<li>Удалить элемент /api/address/delete/$id/<br>
   Тип запроса DELETE</li>
</ul>  