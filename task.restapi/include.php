<?php
Bitrix\Main\Loader::registerAutoloadClasses(
    "task.restapi",
    array(
        "Task\\Restapi\\Router" => "lib/Router.php",
        "Task\\Restapi\\Address" => "lib/Address.php",
    )
);