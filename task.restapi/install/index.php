<?
use \Bitrix\Main\ModuleManager,
    Bitrix\Main\UrlRewriter,
    Bitrix\Main\Localization\Loc;

class Task_restapi extends CModule
{
    var $MODULE_ID = "task.restapi";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "28.09.2020";
        $this->MODULE_NAME = Loc::getMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("MODULE_DESCRIPTION");
    }

    function DoInstall()
    {
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallUrlRewrite();
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UninstallUrlRewrite();
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/install.sql");
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/insert.sql");
        if (!$this->errors) {
            return true;
        } else
            return $this->errors;
    }

    function UnInstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/uninstall.sql");
        if (!$this->errors) {
            return true;
        } else
            return $this->errors;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/section/",
            $_SERVER["DOCUMENT_ROOT"] . "/",
            true, true
        );
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx( "/api");
        return true;
    }

    function InstallUrlRewrite()
    {
        $siteId = \CSite::GetDefSite();
        UrlRewriter::add(
            $siteId,
            array(
                "CONDITION" => "#^/api/#",
                "ID" => "task.restapi",
                "PATH" => "/api/index.php",
                "RULE" => ""
            )
        );
    }

    function UninstallUrlRewrite()
    {
        $siteId = \CSite::GetDefSite();
        UrlRewriter::delete(
            $siteId,
            array(
                "ID" => "task.restapi"
            )
        );
    }
}