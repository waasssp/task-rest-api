<?php
$MESS["ELEMENTS_NOT_FOUND"] = "Элементы не найдены";
$MESS["NOT_FOUND"] = "не найден";
$MESS["ELEMENT_WITH"] = "Элемент с";
$MESS["ADDED_SUCCESS"] = "успешно добавлен";
$MESS["ADDED_ERROR"] = "Ошибка добавления:";
$MESS["ADDED_ERROR_NO_FIELDS"] = "Ошибка добавления: не переданы обязательные поля";
$MESS["UPDATE_SUCCESS"] = "успешно обновлен";
$MESS["UPDATE_ERROR"] = "Ошибка обновления:";
$MESS["UPDATE_ERROR_NOT_FOUND"] = "Ошибка обновления: элемент не найден";
$MESS["DELETE_SUCCESS"] = "успешно удален";
$MESS["DELETE_ERROR"] = "Ошибка удаления:";
$MESS["DELETE_ERROR_NOT_FOUND"] = "Ошибка удаления: элемент не найден";