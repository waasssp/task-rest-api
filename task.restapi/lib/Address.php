<?php

namespace Task\Restapi;

use Bitrix\Main\Application,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Localization\Loc;

/**
 * Class Address
 * @package Task\Restapi
 */
class Address
{
    /**
     * Returns elements list.
     *
     * @return array
     */
    public static function getAll()
    {
        $result = AddressTable::getList(
            array(
                'select' => array('*')
            ));
        while ($row = $result->fetch()) {
            $row["CREATED_AT"] = $row["CREATED_AT"]->toString();
            $row["UPDATED_AT"] = $row["UPDATED_AT"]->toString();
            $arResult[] = $row;
        }
        if (!empty($arResult)) {
            $response = $arResult;
        } else {
            $response = ["error" => Loc::getMessage("ELEMENTS_NOT_FOUND")];
        }
        return $response;
    }

    /**
     * @param $id
     *
     * Returns element by Id
     *
     * @return array
     */
    public static function getById($id)
    {
        $result = AddressTable::getById($id);
        if ($row = $result->fetch()) {
            $row["CREATED_AT"] = $row["CREATED_AT"]->toString();
            $row["UPDATED_AT"] = $row["UPDATED_AT"]->toString();
            $arResult[] = $row;
            $response = $row;
        } else {
            $response = ["error" => Loc::getMessage("ELEMENT_WITH") . " id " . $id . " " . Loc::getMessage("NOT_FOUND")];
        }
        return $response;
    }

    /**
     * Add new element
     *
     * @return array
     */
    public static function add()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $name = $request->getPost("name");
        $address = $request->getPost("address");
        if (!empty($name) && !empty($address)) {
            $arFields = array(
                "NAME" => $name,
                "ADDRESS" => $address
            );
            $result = AddressTable::add($arFields);
            $id = $result->getId();
            if ($id) {
                $response = ["success" => Loc::getMessage("ELEMENT_WITH") . " id " . $id . " " . Loc::getMessage("ADDED_SUCCESS")];
            } else {
                $response = ["error" => Loc::getMessage("ADDED_ERROR") . " " . $result->getErrorMessages()];
            }
        } else {
            $response = ["error" => Loc::getMessage("ADDED_ERROR_NO_FIELDS")];
        }
        return $response;
    }

    /**
     * Update element by Id
     *
     * @return array
     */
    public static function update()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $id = $request->getPost("id");
        $name = $request->getPost("name");
        $address = $request->getPost("address");
        $needUpdate = self::getById($id);

        if (empty($needUpdate["error"])) {
            $arFields = array(
                "NAME" => (!empty($name)) ? $name : $needUpdate["NAME"],
                "ADDRESS" => (!empty($address)) ? $name : $needUpdate["ADDRESS"],
                "CREATED_AT" => $needUpdate["CREATED_AT"],
                "UPDATED_AT" => DateTime::createFromTimestamp(time())
            );
            $result = AddressTable::update($id, $arFields);
            if ($result->isSuccess()) {
                $response = ["success" => Loc::getMessage("ELEMENT_WITH") . " id " . $id . " " . Loc::getMessage("UPDATE_SUCCESS")];
            } else {
                $response = ["error" => Loc::getMessage("UPDATE_ERROR") . " " . $result->getErrorMessages()];
            }
        } else {
            $response = ["error" => Loc::getMessage("UPDATE_ERROR_NOT_FOUND")];
        }
        return $response;
    }

    /**
     * @param $id
     *
     * Delete element by Id
     *
     * @return array
     */
    public static function delete($id)
    {
        $needUpdate = self::getById($id);
        if (empty($needUpdate["error"])) {
            $result = AddressTable::delete($id);
            if ($result->isSuccess()) {
                $response = ["success" => Loc::getMessage("ELEMENT_WITH") . " id " . $id . " " . Loc::getMessage("DELETE_SUCCESS")];
            } else {
                $response = ["error" => Loc::getMessage("DELETE_ERROR") . " " . $result->getErrorMessages()];
            }
        } else {
            $response = ["error" => Loc::getMessage("DELETE_ERROR_NOT_FOUND")];
        }
        return $response;
    }
}