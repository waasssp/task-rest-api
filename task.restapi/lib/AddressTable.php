<?php
namespace Task\Restapi;

use Bitrix\Main\Entity,
    Bitrix\Main\Localization\Loc;


/**
 * Class AddressTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string mandatory
 * <li> ADDRESS string mandatory
 * <li> CREATED_AT datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> UPDATED_AT datetime mandatory default 'CURRENT_TIMESTAMP'
 * </ul>
 *
 * @package \Task\Restapi
 **/

class AddressTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'api_table';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
            ),
            'NAME' => array(
                'data_type' => 'text',
                'required' => true,
                'title' => Loc::getMessage('DATA_ENTITY_NAME_FIELD'),
            ),
            'ADDRESS' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_ADDRESS_FIELD'),
            ),
            'CREATED_AT' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_CREATED_FIELD'),
            ),
            'UPDATED_AT' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_UPDATED_FIELD'),
            ),
        );
    }
}