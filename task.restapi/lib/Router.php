<?php

namespace Task\Restapi;

use Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc;

/**
 * Class Router
 * @package Task\Restapi
 */
class Router
{
    public function init()
    {
        $routes = $this->getRouters();

        $request = Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();

        foreach ($routes as $key => $value) {
            if (strpos($uriString, $key) === 0) {
                $class = $value["class"];
                $method = $value["method"];
            }
        }
        if (class_exists(__NAMESPACE__ . "\\" . $class) && method_exists(__NAMESPACE__ . "\\" . $class, $method)) {
            $controllerClassName = __NAMESPACE__ . "\\" . $class;
            $controller = new $controllerClassName;
            $uriArray = explode("/", $uriString);
            $param = $uriArray[4];
            $result = $controller->{$method}($param);
        } else {
            $result = Loc::getMessage("CLASS_OR_METHOD_NOT_EXIST");
        }
        return $result;
    }

    /**
     * Returns list of routes
     * @return array
     */
    public function getRouters()
    {
        return [
            "/api/address/list/" => ["class" => "Address", "method" => "getAll"],
            "/api/address/get/" => ["class" => "Address", "method" => "getById"],
            "/api/address/update/" => ["class" => "Address", "method" => "update"],
            "/api/address/add/" => ["class" => "Address", "method" => "add"],
            "/api/address/delete/" => ["class" => "Address", "method" => "delete"],
            "/api/qwerty/list/" => ["class" => "qwerty", "method" => "delete"],
        ];
    }
}